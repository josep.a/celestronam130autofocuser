include <./constants.scad>;
use <./telescope.scad>;
use <./gear.scad>;
use <./support.scad>;
use <./micro_servo_9g.scad>;
use <./electronics.scad>;

$fn = 20;

color("grey")
rotate([-90, 0, 0])
    telescope();

translate([servo_x_offset, 0, servo_axis_z  - 5.5])
    rotate([180, -90, 0])
        color("cyan", alpha=0.3)
            servo(0);

transform_arduino(){
    arduino();
}

color("black"){
    // servo axis
    translate([bracket_support_size[1]/2, 0, servo_axis_z ])
        rotate([0, 90, 0])
            cylinder(h=200, d=1, center=true);
    // telescope axis
    rotate([0, 90, 0])
        cylinder(h=200, d=1, center=true);
}

module big_gear() {
    difference() {
        union() {
            gear(big_teeths, modu, gear_height);
            translate([0, 0, -gear_height])
                cylinder(h=gear_height + 5, d=44);

        }
        translate([0, 0, -11])
            cylinder(h=10, d=41);
        translate([0, 0, -9])
            cylinder(h=9, d=40);
    }
}

module small_gear() {
    difference() {
        gear(small_teeths, modu, gear_height);
        
        translate([0, 0, gear_height + 0.01]) rotate([0, 180, 0]) scale([1.05, 1.05, 5])
            servo_connector_4arms(show_holes=false);
    }
}

translate([gears_x_offset, 0, 0]) rotate([0, 90, 0])
    big_gear();

// Small gear
translate([gears_x_offset, 0, servo_axis_z]) rotate([0, 90, 0])
    small_gear();

support(servo_axis_z, servo_x_offset);