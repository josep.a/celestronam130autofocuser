//www.elegoo.com
//2016.12.08
#include <Servo.h>

#define SERVO_PIN 9
#define JOYSTICK_PIN A1
#define JOYSTICK_THRESHOLD 40

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  Serial.begin(9600);
  myservo.attach(SERVO_PIN);  // attaches the servo on pin 9 to the servo object
}

void loop() {
  int joystick_y_value = analogRead(JOYSTICK_PIN) - (1024 / 2);
  Serial.println(joystick_y_value);
  if (joystick_y_value < -JOYSTICK_THRESHOLD) {
    pos += 1;
  } else if (joystick_y_value > JOYSTICK_THRESHOLD) {
    pos -= 1;
  }

  pos = min(160, pos);
  pos = max(10, pos);

  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(15);                       // waits 15ms for the servo to reach the position
}
