// Based on: https://gist.github.com/mildsunrise/327a828e59ed97176ab7dbbaabb4d216
// and https://khkgears.net/new/gear-module.html
// and https://khkgears.net/new/gear_knowledge/gear_technical_reference/calculation_gear_dimensions.html

function compute_step(modu) = modu * 3.14 / 2;

function gear_radius(teeth, modu) = 
    let (angle = 360 / (teeth*2))
        (compute_step(modu) / 2) / sin(angle/2);

module gear(teeth, modu, height=0.2) {
    angle = 360/(teeth*2);
    step = compute_step(modu);
    radius = gear_radius(teeth, modu);
    apothem = (step/2) / tan(angle/2);
    
    module circles() {
        for (i = [1:teeth])
            rotate(i * angle * 2) translate([radius,0,0]) circle(step/2);
    }
    
    linear_extrude(height) difference() {
        union() {
            circle(apothem);
            circles();
        }
        rotate(angle) circles();
    }
}
