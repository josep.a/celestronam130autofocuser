use <./gear.scad>;

axis_height = 37.5;

bracket_z_offset = 25.5;
telescope_axis_offset = 5.5 + 2.5;

gear_ratio = 1;
gear_distance = telescope_axis_offset + axis_height + bracket_z_offset;
gear_module = 6.4545;
gear_height = 10;

axis_radius = 5.5;

bracket_hole_offset = 40;
bracket_support_size = [35 + bracket_hole_offset, 30, 4];
bracket_hole_radius = 1.5;
bracket_holes_separation = 20;
bracket_height = axis_height + 15;
holder_y_offset = 22;

arduino_support_angle = -20;

modu = 2;
big_teeths = 32;
small_teeths = 21;
servo_axis_z = gear_radius(big_teeths, modu) + gear_radius(small_teeths, modu);
gears_x_offset = 95/2;
servo_x_offset = gears_x_offset - 15;