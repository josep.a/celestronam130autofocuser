module multiHull() {
    for( i = [1 : $children - 1]) {
        hull(){
            children(0);
            children(i);
        }
    }
}

module sequentialHull(){
    for (i = [0: $children-2]) {
        hull(){
            children(i);
            children(i+1);
        }
    }
}


module corner_cube(index, dims, x_width, y_width) {
    square_corner = [
        [0.5, 0.5],
        [0.5, -0.5],
        [-0.5, -0.5],
        [-0.5, 0.5]
    ];
    
    offset = [
        square_corner[index].x * (dims.x - x_width), 
        square_corner[index].y * (dims.y - y_width), 
        dims.z / 2
    ];
    
    translate(offset)
        cube([x_width, y_width, dims.z], center=true);
    
}
module corner_cylinder(index, radius, dims) {
    square_corner = [
        [0.5, 0.5],
        [0.5, -0.5],
        [-0.5, -0.5],
        [-0.5, 0.5]
    ];
    
    offset = [
        square_corner[index].x * (dims.x- radius * 2), 
        square_corner[index].y * (dims.y - radius * 2), 
        0
    ];
    
    translate(offset)
        cylinder(h=dims.z, r=radius);
}

module cross_base(dims, inner_radius=1, outer_radius=1, center_radius=2, cross_height=1, wall_x=1, wall_y=1) {
    
    inner_dims = [
        dims.x - wall_x * 2 + inner_radius,
        dims.y - wall_y * 2 + inner_radius,
        cross_height
    ];

    multiHull(){
        cylinder(h=cross_height, r=center_radius);
        
        corner_cylinder(0, inner_radius, inner_dims);
        corner_cylinder(1, inner_radius, inner_dims);
        corner_cylinder(2, inner_radius, inner_dims);
        corner_cylinder(3, inner_radius, inner_dims);
    }
    
    sequentialHull() {
        corner_cube(0, dims, wall_x, wall_y);
        corner_cube(1, dims, wall_x, wall_y);
        corner_cube(2, dims, wall_x, wall_y);
        corner_cube(3, dims, wall_x, wall_y);
        corner_cube(0, dims, wall_x, wall_y);
    }
}


module rail(
        rail_dimension,
        rail_depth,
        rail_inner_height,
        rail_height
    ){
        cube(rail_dimension);
        translate([rail_dimension[0] - 0.01, 0, rail_height])
            cube([rail_depth + 0.01, rail_dimension[1], rail_inner_height]);
        translate([-rail_depth + 0.01, 0, rail_height])
            cube([rail_depth + 0.01, rail_dimension[1], rail_inner_height]);
}

module slot(
        rail_dimension,
        wall_width,
        floor_height,
        rail_depth,
        rail_inner_height,
        rail_height,
        tolerance=0.1,
        endstop_size=0
    ){
        difference(){
            dims = rail_dimension + [wall_width * 2, 0, floor_height];
            translate([dims.x / 2, dims.y / 2])
                cross_base(dims, inner_radius=wall_width/2, outer_radius=2,
                                       cross_height=floor_height, wall_x = wall_width, wall_y=wall_width);
            
            translate([wall_width - tolerance, -0.01, floor_height - tolerance])
                rail(rail_dimension + [0.01 + tolerance * 2, 0.02, 0.01 + tolerance],
                        rail_depth,
                        rail_inner_height + tolerance * 2,
                        rail_height);
        }
        if(endstop_size >0 ){
            translate([0, 0.01, floor_height - tolerance - 0.01])
                cube([
                    rail_dimension[0] + wall_width*2 - 0.02,
                    endstop_size,
                    rail_dimension[2] + tolerance
                ]);
        }
}

color("red")
slot(
    rail_dimension=[10, 20, 5],
    rail_depth=2,
    rail_inner_height=2,
    rail_height=2,
    wall_width=3,
    floor_height=1.5
);
/*
color("blue")
translate([3, 0, 1.5])
rail(
    rail_dimension=[10, 20, 5],
    rail_depth=2,
    rail_inner_height=2,
    rail_height=2
);
*/