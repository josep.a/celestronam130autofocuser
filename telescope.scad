tube_diameter = 160;
focus_wheel_diameter = 40;
focus_wheel_width = 14;
focus_gears_inner_separation = 95 - focus_wheel_width*2;

$fn = 100;
module reuleaux(width, height) {
    intersection_for(i = [0 : 120 : 361]){
        rotate([0, 0, i]) translate([width/2, 0, 0])
            cylinder(h=height, r=width);
    }
}


module tube() {
        cylinder(d=tube_diameter, 500);
}

module focuser() {
    intersection(){
        translate([0, 0, -30])
        reuleaux(95, 37);
        translate([-50, 0, -tube_diameter / 2 + 10]) rotate([0, 90, 0])
            tube();
    }

    translate([0, 0, -5])
        cylinder(h=15, d1=80, d2=67);
    translate([0, 0, 10])
        cylinder(h=60, d1=67, d2=46);
}

module focuser_gear() {
    gear_offset = focus_gears_inner_separation / 2 + focus_wheel_width / 2;
    for(i = [-gear_offset, gear_offset]){
        translate([0, 0, i])
            cylinder(h=focus_wheel_width, d=focus_wheel_diameter, center=true);
    }
    cylinder(h=focus_gears_inner_separation + 0.02, d=5, center=true);
     
    distance_from_axis_to_hole_base = 6 - 2.5;
    
    translate([0, -distance_from_axis_to_hole_base, 0]) rotate([90, 90, 0])
    {
        wall_width = 4;
        cube_width = 35;
        cube_depth = 30 + wall_width * 2;
        cube_height = 30;
        top_width = 14;
        hole_depth = 4.5;
        depth_extension_length = 25;
        
        h = 8;
        diff = (cube_width - top_width) / 2;
        angle = atan(h / diff);

        to_remove_cube_width = diff / cos(angle);
        offset_ = to_remove_cube_width * sin(45) * cos(180 + 45 + angle);
        
        translate([- cube_width / 2, -cube_depth / 2, -cube_height + hole_depth])
        difference(){
            cube([cube_width, cube_depth + depth_extension_length, cube_height]);
            
            translate([-offset_, 0, cube_height-offset_]) rotate([0, -angle, 0])
                cube([to_remove_cube_width, 200, to_remove_cube_width], center=true);
            translate([cube_width + offset_, 0, cube_height-offset_]) rotate([0, angle, 0])
                cube([to_remove_cube_width, 200, to_remove_cube_width], center=true);
            
            translate([-0.5, wall_width, cube_height - hole_depth])
                cube([cube_width + 1, cube_depth - wall_width * 2, cube_height + 1]);
        }
    }
}

module telescope() {
    translate([0, 30, -25 - focus_wheel_diameter / 2]){
        translate([-70, 0, -tube_diameter / 2]) rotate([0, 90, 0])
            tube();
        focuser();
    }
    rotate([0, 90, 0])
        focuser_gear();
}

telescope();