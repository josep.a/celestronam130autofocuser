include <./constants.scad>;
use <./micro_servo_9g.scad>;
use <./rails.scad>;
use <./electronics.scad>;

$fn=30;

// Arduino rail constants
rail_floor_height = 3.1;
rail_endstop_size = 2;
rail_tolerance = 1;
arduino_width = 30;
rail_depth = 2;
rail_wall_width = 4;
rail_length = 50;
rail_height = 3;

module transform_arduino() {
    translate([-13, arduino_width / 2, 18])
        rotate([51.5, 0, -90])  // TODO remove this hardcoded angle
            children();
}

module arduino_rail(){
    offset_ = [
        - (rail_wall_width - rail_depth + rail_tolerance / 2),
        0, 0
    ];
    rail_dims = [
        arduino_width + rail_tolerance - 2 * rail_depth,
        rail_length,
        6
    ];
    
    translate(offset_) slot(
        rail_dimension=rail_dims,
        wall_width=rail_wall_width,
        floor_height=rail_floor_height,
        rail_depth=rail_depth,
        rail_inner_height=2,
        rail_height=rail_height,
        tolerance=0.01,
        endstop_size=rail_endstop_size
    );
}

module base(dims, top_width){
    scale_factor = [
        1,
        top_width / dims.y
    ];
    square_part_height = 7;
    
    translate([0, 0, square_part_height / 2])
        cube([dims.x, dims.y, square_part_height], center=true);
    
    translate([0, 0, square_part_height])
        linear_extrude(dims.z - square_part_height, scale=scale_factor)
            square([dims.x, dims.y], center=true);
}

module servo_holder(dims) {
    difference(){
        translate([-dims.x, -dims.y / 2, -dims.z / 2])
            cube(dims);
        
        // Servo axis hole
        translate([0, 0, 5.5]) rotate([0, 90, 0]){
            cylinder(h=100 + 0.01, r=axis_radius + 0.5, center=true);
            translate([5, 0, 0])
                cube([10, 7, 50], center=true);
        }
        
        // Rectangular dent
        servo_square_hole_dims = [dims.x - 4, 13, 23];
        servo_square_hole_offset = [
            -dims.x + servo_square_hole_dims.x / 2 - 0.01, 0, 0
        ];
        translate(servo_square_hole_offset)
            cube(servo_square_hole_dims, center=true);
        
        // Screws
        for(j = [1, -1]) {
            offset_ = servo_square_hole_offset + [0, 0, 14.2 * j];
            translate(offset_)
                rotate([0, 90, 0])
                    cylinder(h=100, d=2, center=true);
        }
    }
}

module support(servo_axis_height, servo_x_offset, render_arduino=false) {
    support_base_dims = [30, 29, 10];
    servo_bracket_dims = [6.8, 20, 45];
    servo_holder_extension = 8;
    
    difference(){
        union(){
            base(support_base_dims, servo_bracket_dims.y);
        
            translate([servo_x_offset + 15, 0, servo_axis_height - 5.5]){
                servo_holder(servo_bracket_dims);
                
                translate([-servo_bracket_dims.x - servo_holder_extension / 2, 0, 0])
                    cube([
                        servo_holder_extension,
                        servo_bracket_dims.y,
                        servo_bracket_dims.z], center=true);
            }
                
            // join the two parts
            hull(){
                translate([0, 0, support_base_dims.z - 0.01])
                    cube([support_base_dims.x, servo_bracket_dims.y, 0.01], center=true);
                translate([
                        servo_x_offset + 15 - servo_bracket_dims.x + 0.01 - servo_holder_extension,
                        0,
                        servo_axis_height - 5.5
                    ])
                    cube(servo_bracket_dims - [servo_bracket_dims.x - 0.01, 0, 0], center=true);
            }
            
            // Arduino
            transform_arduino() {
                translate([0, -2, -4.1])
                    arduino_rail();
                if(render_arduino){
                    color("green", alpha=0.1)
                        arduino();
                }
            }
        }
        
        union() {
            // Servo dent
            servo_dent_dims = [100, 15, 40.01];
            
            servo_dent_position = [
                servo_x_offset + 15 - servo_dent_dims.x - servo_bracket_dims.x + 0.02,
                -servo_dent_dims.y / 2,
                (servo_axis_height - 5.5) - 35 / 2
            ];
            translate(servo_dent_position)
                cube(servo_dent_dims);
        
            // Base screw holes
            for(y = [10, 10 - bracket_holes_separation]) {
                translate([0, y, -0.01]) {
                    cylinder(h=5, r=bracket_hole_radius + 0.2);
                    translate([0, 0, 1])
                        cylinder(h=100, r=3.5);
                }
            }
        }
    }
}
/*
translate([servo_x_offset, 0, servo_axis_z - 5.5]) rotate([180, -90, 0])
color("cyan", alpha=0.2)
    servo(0);
*/

support(servo_axis_z, servo_x_offset, render_arduino=false);
//arduino_rail();
/*
intersection(){
    #support(servo_axis_z, servo_x_offset, render_arduino=false);
    
    union(){
        translate([-30, 0, 0])
            cube([80, 35, 150]);
        translate([0, 0, 5])
            cube([30, 29, 10], center=true);
    }
}
*/