
module arduino() {
    translate([30, 70, 0]) rotate([0, 0, 180]){
        color("green")
            translate([0, 0, 2])
                cube([30, 70, 2]);
        
        color("black")
            translate([22, 4, 4])
                cube([18, 11, 12]);
        
        color("blue")
            translate([2, 0, 4])
                cube([18, 45, 8]);
        
        color("black")
            translate([28 - 8, 70 - 8, 4])
                cube([8, 2.5, 10]);
        
        color("grey")
            translate([2, 2, 0])
            cube([26, 66, 2]);
    }
}

arduino();