module servo(angle){
    width = 22.5;
    depth = 12.5;
    height = 22;
    support_offset = 16;
    support_height = 2.5;
    support_width = 32.5;
    
    difference(){
        union(){
            cube([width, depth, height], center=true);
            translate([0, 0, support_offset / 2 - support_height / 2])
                cube([support_width, depth + 0.01, support_height], center=true);
            
            translate([width/2 - 11.5/2, 0, height / 2 - 0.1])
                cylinder(4, d=11.5);
        }
        union(){
            for(i = [1, -1]){
                translate([i * (support_width / 2 - 1), 0, support_offset / 2])
                    cylinder(10, d=2, center=true);
            }
        }
    }
}

module servo_connector_4arms(show_holes = true) {
    $fn=30;
    left_arm_length = 14.25;
    right_arm_length = 16.25;
    width = 12.7;
    circles_radius = 2;
    holes_offset = 1.5;
    height = 1.5;
    
    points_clockwise = [
        [left_arm_length + circles_radius, 7/2],
        [left_arm_length + circles_radius, -7/2],
        [left_arm_length - circles_radius, -6/2],
        [left_arm_length - circles_radius, 6/2]
    ];
    
    difference(){
        union(){
            translate([-left_arm_length, 0, 0]){
                linear_extrude(height=height){
                    difference(){
                        // BODY
                        union(){
                            hull(){
                                polygon(points_clockwise);
                                circle(r=2);
                                translate([left_arm_length + right_arm_length, 0, 0])
                                    circle(r=2);
                            }
                            hull(){
                                translate([left_arm_length, 6.35, 0])
                                    circle(r=2);
                                translate([left_arm_length, -6.35, 0])
                                    circle(r=2);
                            }
                        }
                        if (show_holes){
                            // HOLES
                            union(){
                                // arm left
                                for(i = [0:5])
                                    translate([i * holes_offset, 0, 0])
                                        circle(d=1);
                                
                                // arm right
                                for(i = [0:6])
                                    translate([left_arm_length + right_arm_length - i * holes_offset, 0, 0])
                                        circle(d=1);
                                // arm top
                                for(i = [0:1])
                                    translate([left_arm_length, width / 2 - i * holes_offset, 0])
                                        circle(d=1);
                                // arm bottom
                                for(i = [0:1])
                                    translate([left_arm_length, - width / 2 + i * holes_offset, 0])
                                        circle(d=1);
                            }
                        }
                    }
                }
            }
            translate([0, 0, height]){
                difference(){
                    cylinder(h=4 - height, d=7);
                    if(show_holes){
                        translate([0, 0, 0.01])
                            cylinder(h=4 - height, d=5);
                    }
                }
            }
        }
        if(show_holes){
            translate([0, 0, -0.1])
                cylinder(h=10, d=2.5);
        }
    }
}

color("blue")
    servo();

translate([5.5, 0, 20])
    rotate([0, 180, 0])
    servo_connector_4arms();